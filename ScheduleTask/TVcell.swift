//
//  TVcell.swift
//  ScheduleTask
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import ObjectMapper

class TVcell: UITableViewCell
{
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblViewForData: UIView!
    
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lnlPlace: UILabel!
    @IBOutlet weak var lblEnrolled: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblPerStudent: UILabel!
   
    @IBOutlet weak var lblEnrolledHeading: UILabel!
    @IBOutlet weak var lblAgeHeading: UILabel!
    @IBOutlet weak var lblPerSubjectHeading: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
