//
//  CVcellCollectionViewCell.swift
//  ScheduleTask
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class CVcellCollectionViewCell: UICollectionViewCell {
    
    var dataForTableCellFromDetails : [Details]?
    //var dataForTableCellFromLocations : [Locations]!
    //var dataForTableCellFromSubjects : [Subjects]!
    @IBOutlet weak var TableView: UITableView!

    override func awakeFromNib() {
        super.awakeFromNib()
        TableView.delegate = self
        TableView.dataSource = self
        
        }
}

extension CVcellCollectionViewCell : UITableViewDataSource  , UITableViewDelegate
{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (dataForTableCellFromDetails?.count)!
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let tabCell = tableView.dequeueReusableCell(withIdentifier: "tabCell", for: indexPath) as! TVcell
        
        tabCell.lblTime?.text = (dataForTableCellFromDetails?[indexPath.row].start_time)!
        tabCell.lblHours?.text = dataForTableCellFromDetails?[indexPath.row].time_duration
        tabCell.lblHome?.text = "Home"
        tabCell.lnlPlace?.text = dataForTableCellFromDetails?[indexPath.row].day_location
        tabCell.lblEnrolled?.text = String(describing: dataForTableCellFromDetails?[indexPath.row].enrolled ?? 0)
        tabCell.lblAge?.text = dataForTableCellFromDetails?[indexPath.row].age_group
        tabCell.lblPerStudent.text = (dataForTableCellFromDetails?[indexPath.row].charge_student)!
        
        
        tabCell.lblViewForData.layer.shadowColor = UIColor.lightGray.cgColor
        tabCell.lblViewForData.layer.shadowOpacity = 1
        tabCell.lblViewForData.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        tabCell.lblViewForData.layer.shadowRadius = 2
        
        
        for subject in (dataForTableCellFromDetails?[indexPath.row].subjects)!
            
        {
            tabCell.lblSubject?.text = subject.subject_name ?? ""
        }
        return tabCell
    }
    
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 5
    }
 }

