//
//  ViewController.swift
//  ScheduleTask
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SVProgressHUD

class ViewController: UIViewController {

    
    @IBOutlet weak var lblHeadDate: UILabel!
    
    @IBOutlet weak var lblDay1: UILabel!
    @IBOutlet weak var lblDay2: UILabel!
    @IBOutlet weak var lblDay3: UILabel!
    @IBOutlet weak var lblDay4: UILabel!
    @IBOutlet weak var lblDay5: UILabel!
    @IBOutlet weak var lblDay6: UILabel!
    @IBOutlet weak var lblDay7: UILabel!
    
    @IBOutlet weak var CollectionView: UICollectionView!
    
    var flagDaySet : Double = 0
    var sending : Instructor?
    var userModel : Instructor?
    
    var tomDate = Date()
    var lastDateInScroll : String?
    var NextDateInScroll : String?
    var CompleteDate : String?
    var monthDateValue : String?
    var NextDateDisplay = Date()

    
    //var Day1BoldDone : Double = 0
    //var flagDaySet : Double = 0
    //var flagDaySet : Double = 0
   // var flagDaySet : Double = 0
   // var flagDaySet : Double = 0
   // var flagDaySet : Double = 0
   // var flagDaySet : Double = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Alamofire.request("http://34.195.206.185/api/login")
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        super.viewDidLoad()
        
        let yearMonthDateFormat = DateFormatter()
        yearMonthDateFormat.dateFormat = "yyyy-MM-dd"
        CompleteDate = yearMonthDateFormat.string(from: Date())
        
        let monthDateFormat = DateFormatter()
        monthDateFormat.dateFormat = "MMM-dd"
        monthDateValue = monthDateFormat.string(from: Date())
        
        let todayDate = Date()
        
        
        
        
        let tomorrowDateFormat = DateFormatter()
        tomorrowDateFormat.dateFormat = "yyyy-MM-dd"
        tomDate = tomorrowDateFormat.calendar.date(byAdding: .day, value: 6,to: todayDate)!
        lastDateInScroll = tomorrowDateFormat.string(from: tomDate)
        
      /*  let CurrentDateForHead = DateFormatter()
        CurrentDateForHead.dateFormat = "yyyy-MM-dd"
        NextDateDisplay = CurrentDateForHead.calendar.date(byAdding: .day, value: 1,to: NextDateDisplay)!
        NextDateInScroll = CurrentDateForHead.string(from: NextDateDisplay) */
        

        
        lblHeadDate.text = "Today, " + monthDateValue!
        
        Alamofire.request("http://34.195.206.185/api/instructor-home")
        

        fetchData()
    }
    
    
    func getDayOfWeekInShortString(today:String)->String?
    {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today)
        {
            let Calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let Components = Calendar.components(.weekday, from: todayDate)
            let weekDay = Components.weekday
            switch (weekDay!)
            {
            case 1:
                return "Sun"
            case 2:
                return "Mon"
            case 3:
                return "Tue"
            case 4:
                return "Wed"
            case 5:
                return "Thu"
            case 6:
                return "Fri"
            case 7:
                return "Sat"
            default:
                return " "
            }
        }
        return nil
    }
    
    
    
    
    
    /*
    
    func BoldDayOfWeek(today:Int)
    {
            switch (today)
            {
            case 1:
                lblDay1.font = UIFont.boldSystemFont(ofSize: 18)
            case 2:
                lblDay2.font = UIFont.boldSystemFont(ofSize: 18)

            case 3:
                lblDay3.font = UIFont.boldSystemFont(ofSize: 18)

            case 4:
                lblDay4.font = UIFont.boldSystemFont(ofSize: 18)

            case 5:
                lblDay5.font = UIFont.boldSystemFont(ofSize: 18)

            case 6:
                lblDay6.font = UIFont.boldSystemFont(ofSize: 18)

            case 7:
                lblDay7.font = UIFont.boldSystemFont(ofSize: 18)

            default:
                lblDay1.font = UIFont.boldSystemFont(ofSize: 18)
                
            }
    }
*/
    func boldLabelSetting(Daylabel:UILabel)
    {
        var lblDaysArray = [lblDay1, lblDay2, lblDay3, lblDay4, lblDay5, lblDay6, lblDay7]
        for index in 0..<lblDaysArray.count
        {
            if(Daylabel == lblDaysArray[index])
            {
                Daylabel.font=UIFont.systemFont(ofSize: 17, weight: 4)
                
            }
            else
            {
                lblDaysArray[index]?.font=UIFont.systemFont(ofSize: 14, weight: 0)
            }
        }
    }
    
    
    
    
    

    
    
    func fetchData()
    {
        let param:[String:Any] = ["access_token" : "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8" , "page_no": "1" ,"page_size" : "7" ,"date_selected" :"2017-02-23"]
        
        ApiHandler.fetchData(urlStr: "instructor-home", parameters: param)
        {
            (jsonData) in
            // print(jsonData)
            self.userModel = Mapper<Instructor>().map(JSONObject: jsonData)
            self.sending = Mapper<Instructor>().map(JSONObject: jsonData)
            //print(self.sending?.idata?[0].date ?? "")
            //self.lblHeadDate.text = self.sending?.idata?[0].date ?? ""
            
            /*self.lblDay1.text = self.sending.idata?[0].day ?? ""
            self.lblDay2.text = self.sending.idata?[1].day ?? ""
            self.lblDay3.text = self.sending.idata?[2].day ?? ""
            self.lblDay4.text = self.sending.idata?[3].day ?? ""
            self.lblDay5.text = self.sending.idata?[4].day ?? ""
            self.lblDay6.text = self.sending.idata?[5].day ?? ""
            self.lblDay7.text = self.sending.idata?[6].day ?? ""*/
            
             self.lblDay1.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[0].date1!)!)
             self.lblDay2.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[1].date1!)!)
             self.lblDay3.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[2].date1!)!)
             self.lblDay4.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[3].date1!)!)
             self.lblDay5.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[4].date1!)!)
             self.lblDay6.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[5].date1!)!)
             self.lblDay7.text = self.getDayOfWeekInShortString(today:(self.userModel?.idata?[6].date1!)!)

            
            
            //print( (jsonData))
            print(self.userModel?.msg ?? "")
            
            print(self.userModel?.idata?[0].details?[0].subjects?[0].subject_name ?? "")
            print(self.userModel?.idata?[0].details?[0].subjects?[0].subject_id ?? "")
            
            print(self.userModel?.idata?[0].details?[0].enrolled ?? "")
            print(self.userModel?.idata?[0].details?[0].age_group ?? "")
            print(self.userModel?.idata?[0].details?[0].charge_student ?? "")
            print(self.userModel?.idata?[0].details?[0].time_duration ?? "")
            print(self.userModel?.idata?[0].details?[0].start_time1 ?? "")
            
            print(self.userModel?.idata?[0].details?[0].locations?[0].slot_location ?? "")
            print(self.userModel?.idata?[0].details?[0].locations?[0].slot_day ?? "")
           
            self.CollectionView.reloadData()
            }
    }
}
//MARK:- CollectionView Delegates/ DataSource
extension ViewController : UICollectionViewDataSource  , UICollectionViewDelegate , UIScrollViewDelegate, UICollectionViewDelegateFlowLayout
        {
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
            {
                return userModel?.idata?.count ?? 0
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
            {
                return 0.0
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
            {
                return 0.0
            }
    
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
            {
                let widthOfCell = self.CollectionView.frame.width
                let HeightOfCell = self.CollectionView.frame.height
                return CGSize(width: widthOfCell, height: HeightOfCell)
            }
            func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
            {
                var visibleRect = CGRect()
                visibleRect.origin = CollectionView.contentOffset
                visibleRect.size = CollectionView.bounds.size
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let visibleIndexPath : IndexPath = CollectionView.indexPathForItem(at: visiblePoint)!
                let lastElement = (userModel?.idata?.count)! - 1
                lblHeadDate.text = "Today, " + monthDateValue!
                if visibleIndexPath.row == lastElement
                {
                    Alamofire.request("http://34.195.206.185/api/instructor-home")
                    let param: [String:Any] = ["access_token": "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8", "page_no": "1", "page_size": "7","last_date": lastDateInScroll ?? "", "date_selected": CompleteDate!]
                    ApiHandler.fetchData(urlStr: "instructor-home", parameters: param)
                    {
                        (jsonData) in
                        let userModelAppendMorePages = Mapper<Instructor>().map(JSONObject: jsonData)
                        for index in 0..<7
                        {
                            self.userModel?.idata?.append((userModelAppendMorePages?.idata?[index])!)
                        }
                        self.CollectionView.reloadData()
                    }
                }
            }
          func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
            {
                
                //self.BoldDayOfWeek(today:indexPath.row)
                    //self.UnBoldDayOfWeek(today:indexPath.row)
                    
                if(indexPath.item % 7 == 0)
                {

                    self.boldLabelSetting(Daylabel:lblDay1)
                    
                }
                else if(indexPath.item % 7 == 1)
                {
                    self.boldLabelSetting(Daylabel:lblDay2)
                }

                else if(indexPath.item % 7 == 2)
                {
                    self.boldLabelSetting(Daylabel:lblDay3)
                }
                else if(indexPath.item % 7 == 3)
                {
                    self.boldLabelSetting(Daylabel:lblDay4)
                }
                else if(indexPath.item % 7 == 4)
                {
                    self.boldLabelSetting(Daylabel:lblDay5)
                }
                else if(indexPath.item % 7 == 5)
                {
                    self.boldLabelSetting(Daylabel:lblDay6)
                }
                else if(indexPath.item % 7 == 6)
                {
                    self.boldLabelSetting(Daylabel:lblDay7)
                }
                
                
                
            }
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
            {
                let colViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "colViewCell", for: indexPath) as! CVcellCollectionViewCell
                colViewCell.dataForTableCellFromDetails = (userModel?.idata?[indexPath.row].details)!
                //colViewCell.dataForTableCellFromLocations = (userModel?.idata?[indexPath.row].details?[indexPath.row].locations)!
                //colViewCell.dataForTableCellFromSubjects = (userModel?.idata?[indexPath.row].details?[indexPath.row].subjects)!
                SVProgressHUD.dismiss()
                return colViewCell
            }
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
            {
            }
        }
 
